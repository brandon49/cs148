package hw1;
import java.util.Scanner;
public class UshSure {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("Ticket number? ");
		int ticketNumber = in.nextInt();
		System.out.print("Seats in a row? ");
		int seatsInARow = in.nextInt();
		
		int column = ticketNumber % seatsInARow;
		int row = 0;
		
		for(int i = ticketNumber;i - seatsInARow >= 0;) {
				row ++;
				i -= seatsInARow;
		}
		
		char seatLetter = (char) ('A' + row);
		
		System.out.println("Seat: " + seatLetter + column);
	
		in.close();
	}
}
