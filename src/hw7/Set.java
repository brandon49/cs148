package hw7;

import java.util.ArrayList;

public class Set {
	private ArrayList<String> sets;

	public Set() {
		sets = new ArrayList<String>();
	}
	
	public boolean has(String s) {
		for (int i = 0; i < sets.size(); ++i) {
			if (sets.get(i).toString().equals(s)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void add(String s) {	
		if (!has(s)) {
			sets.add(s);
		}
	}
	
	public String toString() {
		String s = "";
		
		s += sets.get(0);
		for (int i = 1; i < sets.size(); ++i) {
			s += String.format("%n");
			s += sets.get(i);
		}

		return s;
	}
}
