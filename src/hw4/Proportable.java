package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Proportable {
	public static BufferedImage blankImage(int width, double aspectRatio, Color color) {
		int height = (int) (width / aspectRatio);
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				image.setRGB(j, i, color.getRGB());
			}
		}

		return image;
	}

	public static int toHorizontal(BufferedImage image, double proportionX) {
		double value = (image.getWidth() - 1) * proportionX;

		if (value > 0) {
			return (int) Math.round(value);
		} else {
			return (int) -(Math.round(-value));
		}
	}

	public static int toVertical(BufferedImage image, double proportionY) {
		double value = (image.getHeight() - 1) * (1 - proportionY);

		if (value > 0) {
			return (int) Math.round(value);
		} else {
			return (int) -(Math.round(-value));
		}
	}

	public static int clamp(int number, int lowerBound, int upperBound) {
		return Math.min(Math.max(number, lowerBound), upperBound);
	}

	public static void plotRectangle(BufferedImage image, double bottomLeftX, double bottomLeftY, double topRightX,
			double topRightY, Color color) {

		int imageWidth = image.getWidth() - 1;
		int imageHeight = image.getHeight() - 1;

		int startX = clamp(toHorizontal(image, bottomLeftX), 0, imageWidth);
		int startY = clamp(toVertical(image, topRightY), 0, imageHeight);
		int endX = clamp(toHorizontal(image, topRightX), 0, imageWidth);
		int endY = clamp(toVertical(image, bottomLeftY), 0, imageHeight);

		for (int width = startX; width <= image.getWidth(); width++) {
			for (int height = startY; height <= image.getHeight(); height++) {
				if (width <= endX && height <= endY && width >= startX && height >= startY) {
					image.setRGB(width, height, color.getRGB());
				}
			}
		}
	}

	public static void plotCircle(BufferedImage image, double xProportion, double yProportion, double radiusProportion,
			Color color) {

		int imageWidth = image.getWidth() - 1;
		int imageHeight = image.getHeight() - 1;

		int orginX = toHorizontal(image, xProportion);
		int orginY = toVertical(image, yProportion);
		int radius = toHorizontal(image, radiusProportion);

		int startX = clamp(orginX - radius, 0, imageWidth);
		int startY = clamp(orginY - radius, 0, imageHeight);
		int endX = clamp(orginX + radius, 0, imageWidth);
		int endY = clamp(orginY + radius, 0, imageHeight);

		for (int i = startX; i <= endX; ++i) {
			for (int j = startY; j <= endY; ++j) {
				if (Math.sqrt(Math.pow((i - orginX), 2) + Math.pow((j - orginY), 2)) <= radius) {
					image.setRGB(i, j, color.getRGB());
				}
			}
		}
	}

	public static int chessboardDistance(int aX, int aY, int bX, int bY) {
		return Math.max(Math.abs(bX - aX), Math.abs(bY - aY));
	}

	public static double lerp(double valueA, double valueB, double proportion) {
		return (1 - proportion) * valueA + proportion * valueB;
	}

	public static void plotLine(BufferedImage image, double aX, double aY, double bX, double bY, Color color) {
		int startX = toHorizontal(image, aX);
		int startY = toVertical(image, aY);
		int endX = toHorizontal(image, bX);
		int endY = toVertical(image, bY);

		int distance = chessboardDistance(startX, startY, endX, endY);

		for (int t = 0; t <= distance; t++) {
			double p = t / (double) distance;

			int y = (int) Math.round(lerp(startY, endY, p));
			int x = (int) Math.round(lerp(startX, endX, p));

			if (x >= 0 && x < image.getWidth() && y >= 0 && y < image.getHeight()) {
				image.setRGB(x, y, color.getRGB());
			}
		}
	}

	public static BufferedImage plot(File file, int width) throws FileNotFoundException {
		Scanner in = new Scanner(file);

		double aspectRatio = in.nextDouble();
		in.nextLine();
		
		Color color = new Color(in.nextFloat(), in.nextFloat(), in.nextFloat());
		in.nextLine();
		
		BufferedImage image = blankImage(width, aspectRatio, color);

		while (in.hasNextLine()) {
			String command = "";
			if (in.hasNext()) {
				command = in.next();
			} else {
				in.nextLine();
			}

			if (command.equals("color")) {
				color = new Color(in.nextFloat(), in.nextFloat(), in.nextFloat());
				in.nextLine();
			} else if (command.equals("rectangle")) {
				plotRectangle(image, in.nextDouble(), in.nextDouble(), in.nextDouble(), in.nextDouble(), color);
				in.nextLine();
			} else if (command.equals("circle")) {
				plotCircle(image, in.nextDouble(), in.nextDouble(), in.nextDouble(), color);
				in.nextLine();
			} else if (command.equals("line")) {
				plotLine(image, in.nextDouble(), in.nextDouble(), in.nextDouble(), in.nextDouble(), color);
				in.nextLine();
			} else {
				in.nextLine();
			}
		}

		in.close();
		return image;
	}
}
