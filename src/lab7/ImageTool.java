package lab7;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageTool {
	public static void main(String[] args) throws IOException {
		File file = new File("C:\\Users\\HP\\Desktop\\guernica.jpg");
		BufferedImage img = ImageIO.read(file);
		String colorMap = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|()1{}[]?-_+~<>i!lI;:,\\\"^`.";
		
		int width = img.getWidth();
		int height = img.getHeight();

		for(int i = 0;i < height;++i) {
			for(int j = 0;j < width;++j) {
				int currentColor = img.getRGB(j,i);
				Color newColor = new Color(currentColor);
				
				int greyScale = (int) (((0.2126 * (newColor.getRed()) + 0.7152 * (newColor.getGreen()) + 0.0722 * (newColor.getBlue())) / 255) * colorMap.length());
				System.out.print(colorMap.charAt(greyScale));
			}
			
			System.out.println();
		}
		
	}
}
