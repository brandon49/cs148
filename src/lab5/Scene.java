package lab5;

import java.awt.Color;
import java.awt.Graphics2D;

public class Scene {
	public static void main(String[] args) {
		Canvas canvas = new Canvas(512,512);
		Graphics2D graphics = canvas.getGraphics();
		
		graphics.setColor(new Color(50,50,50));
		graphics.setBackground(new Color(50,50,50));
		graphics.fillRect(0, 0, 512, 512);
		
		drawHouse(200,200,200,200,graphics);
		drawMoon(350,0,150,150,graphics);
		drawPumpkin(350,410,70,50,graphics);
		drawPumpkin(280,410,70,50,graphics);
		drawPumpkin(310,425,50,50,graphics);
		drawPumpkin(275,425,40,45,graphics);
		drawGhost(200,300,40,100,graphics);
		drawGhost(150,200,40,100,graphics);
		drawCauldron(50,350,90,90,graphics);
		
		canvas.show();
	}
	
	public static void drawPumpkin(int x, int y, int width, int height, Graphics2D graphics) {
		graphics.setColor(Color.ORANGE);
		graphics.fillOval(x,y,width,height);
		
		graphics.setColor(Color.black);
		graphics.drawOval(x, y, width, height);
		
		graphics.setColor(Color.GREEN);
		int stemX = x + (width / 2) - 3;
		int stemY = y - (height / 2) + 10;
		graphics.fillRect(stemX, stemY, 10, 20);
		
		graphics.setColor(Color.black);
		graphics.drawRect(stemX, stemY, 10, 20);
	}
	
	public static void drawMoon(int x, int y, int width, int height, Graphics2D graphics) {
		graphics.setColor(Color.YELLOW);
		graphics.fillOval(x, y, width, height);
		
		graphics.setColor(Color.black);
		graphics.drawOval(x, y, width, height);
	}
	
	public static void drawGhost (int x, int y, int width, int height, Graphics2D graphics) {
		graphics.setColor(Color.WHITE);
		graphics.fillOval(x, y, width, height);
		
		graphics.setColor(Color.BLACK);
		int ghostEyeX = x + width / 2 - 12;
		int ghostEyeY = y + height / 2 - height / 3;
		int ghostEyeSize = 10;
		graphics.fillOval(ghostEyeX, ghostEyeY, ghostEyeSize, ghostEyeSize);
		graphics.fillOval(ghostEyeX + 12, ghostEyeY, ghostEyeSize, ghostEyeSize);
		
		graphics.setColor(Color.black);
		graphics.drawOval(x, y, width, height);
	}
	
	public static void drawCauldron(int x, int y, int width, int height, Graphics2D graphics) {
		graphics.setColor(new Color(102,51,0));
		graphics.fillOval(x, y, width, height);
		
		graphics.setColor(Color.BLACK);
		graphics.drawOval(x, y, width, height);
		
		graphics.setColor(new Color(153,76,0));
		graphics.fillOval(x, y - 10, width, 30);
		
		graphics.setColor(Color.BLACK);
		graphics.drawOval(x, y - 10, width, 30);
		
		graphics.setColor(new Color(51,25,0));
		graphics.fillOval(x + 10, y - 4, width - 20, 20);
		
		graphics.setColor(Color.BLACK);
		graphics.drawOval(x + 10, y - 4, width - 20, 20);
	}
	
	public static void drawHouse(int x, int y, int width, int height, Graphics2D graphics) {
		graphics.rotate(Math.toRadians(45));
		graphics.setColor(Color.black);
		graphics.fillRect(x + 73, y - 350, width, height);
		graphics.setColor(new Color(100,100,100));
		graphics.drawRect(x + 73, y - 350, width, height);
		
		graphics.rotate(Math.toRadians(-45));
		graphics.setColor(Color.black);
		graphics.fillRect(x, y, width, height);
		
		graphics.setColor(new Color(60,0,0));
		graphics.fillRect(x + (x / 2 - 30), y + 100, width / 3, height / 2);
	}
}