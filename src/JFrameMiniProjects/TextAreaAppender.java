package JFrameMiniProjects;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class TextAreaAppender extends JFrame {
	private final int WIDTH = 400;
	private final int HEIGHT = 420;
	
	JTextField fieldInput;
	JButton buttonSubmit, buttonResetTextArea;
	JTextArea textArea;
	
	public void createView() {
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		
		JLabel label = new JLabel("Enter Text:");
		panel.add(label);
		
		fieldInput = new JTextField(12);
		panel.add(fieldInput);
		
		buttonSubmit = new JButton("Submit");
		buttonSubmit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String s = fieldInput.getText();
				if(!s.isEmpty()) {
					textArea.append(s + "\n");
				}
			}
			
		});
		panel.add(buttonSubmit);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setPreferredSize(new Dimension(375,300));
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		panel.add(scrollPane);
		
		buttonResetTextArea = new JButton("Clear Text Area");
		buttonResetTextArea.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
			}
			
		});
		panel.add(buttonResetTextArea);
		
	}
	
	public TextAreaAppender() {
		createView();
		setTitle("Text Area Appender Demo");
		setSize(new Dimension(WIDTH,HEIGHT));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TextAreaAppender().setVisible(true);;
			}
		});
	}
}
