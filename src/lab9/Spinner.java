package lab9;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Spinner {
	public static void main(String[] args) throws IOException {
		File file = new File(new File(System.getProperty("user.home")), "spinner.gif");
		GifSequenceWriter gsw = new GifSequenceWriter(file, 1, true);

		for (int i = 0; i < 36; ++i) {
			gsw.appendFrame(generateFrame(i));
		}

		gsw.close();
	}

	public static BufferedImage generateFrame(int timestep) {
		BufferedImage image = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) image.getGraphics();

		int orginX = 80;
		int orginY = 80;
		int radius = 60;

		g.setColor(new Color(100, 100, 100));
		g.fillRect(0, 0, 200, 200);

		if (timestep * 10 + 0 <= 120) {
			g.setColor(new Color(255 - (int) (255 * (timestep / 12.0f)), (int) (255 * (timestep / 12.0f)), 0));
		} else if (timestep * 10 + 0 <= 240 && timestep * 10 + 0 >= 120) {
			g.setColor(new Color(0, 255 - (int) (255 * ((timestep - 12) / 12.0f)),
					(int) (255 * ((timestep - 12) / 12.0f))));
		} else {
			g.setColor(new Color((int) (255 * ((timestep - 24) / 12.0f)), 0,
					255 - (int) (255 * ((timestep - 24) / 12.0f))));
		}

		g.fillOval((int) (Math.cos(Math.toRadians(timestep * 10 + 0)) * radius) + orginX,
				(int) (Math.sin(Math.toRadians(timestep * 10 + 0)) * radius) + orginY, 60, 60);
		g.fillOval((int) (Math.cos(Math.toRadians(timestep * 10 + 30)) * radius) + orginX,
				(int) (Math.sin(Math.toRadians(timestep * 10 + 30)) * radius) + orginY, 60, 60);
		g.fillOval((int) (Math.cos(Math.toRadians(timestep * 10 + 60)) * radius) + orginX,
				(int) (Math.sin(Math.toRadians(timestep * 10 + 60)) * radius) + orginY, 60, 60);

		Area a = new Area(new Rectangle(0, 0, 200, 200));

		for (int i = 0; i < 360; i += 5) {
			int x = (int) (Math.cos(Math.toRadians(i)) * radius) + orginX;
			int y = (int) (Math.sin(Math.toRadians(i)) * radius) + orginY;

			a.subtract(new Area(new Ellipse2D.Double(x + 8, y + 10, 40, 40)));
		}

		g.setColor(new Color(0, 0, 0));
		g.fill(a);

		return image;
	}
}
