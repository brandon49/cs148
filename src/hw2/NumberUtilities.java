package hw2;

public class NumberUtilities {
	public static int round10(double value) { 
		double remainder = value % 10;
		
		if (value >= 0) {
			if (remainder >= 5) {
				double difference = 10 - remainder;
				value += difference;
			} else {
				value -= remainder;
			}
		} else {
			if (remainder >= -5) {
				double difference = 0 - remainder;
				value += difference;
			} else {
				value -= 10 + remainder;
			}
		}
		
		return (int) value;
	}
	
	public static int getGameCount(int roundsInTournement) { 
		int amountOfGames = 0;
		
		for(int i = 0; i < roundsInTournement; i++) {
			amountOfGames = amountOfGames * 2 + 1;
		}
	
		return amountOfGames;
	}
	
	public static double getFraction(double value) {
		double fraction = Math.abs(value % 1);
		
		return fraction;
	}
}
