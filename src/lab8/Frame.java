package lab8;

import java.util.Scanner;

public class Frame {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("Width: ");
		int width = in.nextInt();
		
		System.out.print("Height: ");
		int height = in.nextInt();
		
		System.out.print("Border: ");
		int border = in.nextInt();

		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				if (i < border || j < border || i > (height - 1) - border || j > (width - 1) - border) {
					System.out.print("*");
				} else {
					System.out.print(".");
				}
			}
			System.out.println();
		}
		in.close();
	}
}
