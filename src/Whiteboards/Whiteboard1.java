package Whiteboards;

public class Whiteboard1 {
	// The State (Should be private)
	// How to Initialize that State
	// What Behaviors to Provide

	// This is the State
	private int nPatties;

	public Whiteboard1(int initialPattyCount) {
		nPatties = initialPattyCount;
	}

	public int getPattyCount() {
		return nPatties;
	}

	public int getBunCount() {
		return nPatties + 1;
	}

	public int getBaconCount() {
		return nPatties * 3;
	}

	public int getCheeseCount() {
		return nPatties;
	}

	public int getCalories() {
		return getPattyCount() * 211 + getBunCount() * 27 + getCheeseCount() * 31 + getBaconCount() * 41;
	}
}
