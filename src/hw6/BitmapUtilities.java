package hw6;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

public class BitmapUtilities {
	public static boolean[][] create(int width, int height) {
		return new boolean[height][width];
	}

	public static void randomize(boolean[][] bitmap, long seed) {
		Random rand = new Random(seed);

		for (int i = 0; i < bitmap.length; ++i) {
			for (int j = 0; j < bitmap[i].length; ++j) {
				bitmap[i][j] = rand.nextBoolean();
			}
		}
	}

	public static void write(boolean[][] bitmap, File file) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(file);

		pw.println("P1");
		pw.println(bitmap[0].length + " " + bitmap.length);

		for (int i = 0; i < bitmap.length; ++i) {
			for (int j = 0; j < bitmap[i].length; ++j) {
				if (!bitmap[i][j]) {
					pw.println('1');
				} else {
					pw.println('0');
				}
			}
		}
		pw.flush();
		pw.close();
	}

	public static BufferedImage toBufferedImage(boolean[][] bitmap) {
		int width = bitmap[0].length;
		int height = bitmap.length;

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				if (!bitmap[j][i]) {
					image.setRGB(i, j, Color.WHITE.getRGB());
				} else {
					image.setRGB(i, j, Color.BLACK.getRGB());
				}
			}
		}

		return image;
	}

	public static boolean equals(boolean[][] bitmapOne, boolean[][] bitmapTwo) {
		if (bitmapOne.length == bitmapTwo.length && bitmapOne[0].length == bitmapTwo[0].length) {
			boolean isIdentical = true;

			for (int i = 0; i < bitmapOne.length; ++i) {
				for (int j = 0; j < bitmapOne[i].length; ++j) {
					if (bitmapOne[i][j] != bitmapTwo[i][j]) {
						isIdentical = false;
					}
				}
			}

			return isIdentical;
		}

		return false;
	}

	public static boolean[][] clone(boolean[][] bitmap) {
		boolean[][] newBitmap = new boolean[bitmap.length][bitmap[0].length];

		for (int i = 0; i < bitmap.length; ++i) {
			for (int j = 0; j < bitmap[i].length; ++j) {
				newBitmap[i][j] = bitmap[i][j];
			}
		}

		return newBitmap;
	}

	public static int wrapIndex(int upperBound, int index) {
		return Math.floorMod(index, upperBound);
	}

	public static boolean isOn(boolean[][] bitmap, int columnIndex, int rowIndex) {
		return bitmap[wrapIndex(bitmap.length, rowIndex)][wrapIndex(bitmap[0].length, columnIndex)];
	}
}
