package lab11;

public class Room {
	private String description;
	private Room north;
	private Room east;
	private Room south;
	private Room west;

	public Room(String description) {
		this.description = description;
	}

	public String getRoomDescription() {
		return description;
	}

	public void setNorth(Room rm) {
		north = rm;
	}

	public void setEast(Room rm) {
		east = rm;
	}

	public void setSouth(Room rm) {
		south = rm;
	}

	public void setWest(Room rm) {
		west = rm;
	}

	public Room getNorth() {
		return north;
	}

	public Room getEast() {
		return east;
	}

	public Room getSouth() {
		return south;
	}

	public Room getWest() {
		return west;
	}
}
