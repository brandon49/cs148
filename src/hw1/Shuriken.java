package hw1;
import java.util.Scanner;
public class Shuriken {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int maxDegreeInterval = 360; //The total spin in degrees
		int DegreeInterval = 30; //Increments to go up by
		double pointX; //The x point for the coordinates
		double pointY; //The y point for the coordinates
		
		System.out.print("Spin? ");
		double howManyDegreesToSpin = in.nextDouble();

		for(int i = 0; i <= maxDegreeInterval;) {
			pointX = (1.1 + Math.sin(4*Math.toRadians(i))) * (Math.cos(Math.toRadians(i) + Math.toRadians(howManyDegreesToSpin)));
			pointY = (1.1 + Math.sin(4*Math.toRadians(i))) * (Math.sin(Math.toRadians(i) + Math.toRadians(howManyDegreesToSpin)));
			System.out.printf("%.2f,%.2f", pointX,pointY);
			System.out.println("");
			i += DegreeInterval;
			
		}
		in.close();		
		
	}
}
