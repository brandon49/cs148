package References;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class DesktopClass {
	public static void main(String[] args) throws IOException, URISyntaxException {
		Desktop desktop = Desktop.getDesktop();
		desktop.browse(new URI("/Users/brandonpessman/Desktop")); //Change file to something that is openable in a browser
	}
}
