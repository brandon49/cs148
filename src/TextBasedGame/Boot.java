package TextBasedGame;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Boot {
	private final int WIDTH = 700;
	private final int HEIGHT = 400;
	
	public Boot() {
		Window gameWindow = new Window();
		gameWindow = setupWindow(gameWindow);
		
		//Game Loop Goes Here
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Boot();
			}			
		});
	}
	
	public Window setupWindow(Window gameWindow) {
		gameWindow.setTitle("A Harry Potter Text Game (VERSION 0.0.1)");
		gameWindow.setSize(new Dimension(WIDTH,HEIGHT));
		gameWindow.setLocationRelativeTo(null);
		gameWindow.setResizable(false);	
		gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameWindow.setVisible(true);
		
		return gameWindow;
	}
}
