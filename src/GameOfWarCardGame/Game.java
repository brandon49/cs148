package GameOfWarCardGame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

public class Game extends JFrame {
	Border border;
	private static final long serialVersionUID = 1L;
	private final int WIDTH = 400;
	private final int HEIGHT = 150;

	private ArrayList<String> playerCards = new ArrayList<>();
	private ArrayList<String> cpuCards = new ArrayList<>();
	private ArrayList<String> cardZone = new ArrayList<>();
	private ArrayList<String> initDeck = new ArrayList<>();

	private String[] suit = { "\u2665", "\u2663", "\u2660", "\u2666" };
	private String[] rank = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };

	private JLabel totalCardsPlayer, totalCardsCpu, playerHand, cpuHand, playerWin, cpuWin;
	private JButton resetGame;

	public Game() {
		createView();

		// Sets the title of the window
		setTitle("Game of War (Version 0.2.0)");

		// Sets the size of the window
		setSize(new Dimension(WIDTH, HEIGHT));

		// Sets the close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Sets the location relative to no component
		setLocationRelativeTo(null);

		// Can you resize the window
		setResizable(false);
	}

	public void createView() {
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout());

		JPanel southPanel = new JPanel();
		// southPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		panel.add(southPanel, BorderLayout.SOUTH);

		JPanel northPanel = new JPanel();
		northPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		panel.add(northPanel, BorderLayout.NORTH);

		JPanel westPanel = new JPanel();
		// westPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		panel.add(westPanel, BorderLayout.WEST);

		JPanel eastPanel = new JPanel();
		// eastPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		panel.add(eastPanel, BorderLayout.EAST);

		JPanel centerPanel = new JPanel();
		panel.add(centerPanel, BorderLayout.CENTER);

		playerHand = new JLabel();
		centerPanel.add(playerHand);

		cpuHand = new JLabel();
		centerPanel.add(cpuHand);

		JButton startButton = new JButton("Start");
		startButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (playerCards.size() == 0) {
					createDeck();
					updateWindow();

				}
			}
		});
		southPanel.add(startButton);

		JButton playCardButton = new JButton("Play Card");
		playCardButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (playerCards.size() != 0) {
					try {
						playHand();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					updateWindow();
				} else {
					resetGame();
				}
			}

		});
		southPanel.add(playCardButton);

		resetGame = new JButton("Reset");
		resetGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resetGame();
			}

		});
		southPanel.add(resetGame);

		JButton quitGameButton = new JButton("Quit");
		quitGameButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});
		southPanel.add(quitGameButton);

		totalCardsPlayer = new JLabel("Player Total Cards: " + playerCards.size());
		westPanel.add(totalCardsPlayer);

		totalCardsCpu = new JLabel("CPU Total Cards: " + cpuCards.size());
		eastPanel.add(totalCardsCpu);

		JLabel gameTitleLabel = new JLabel("Welcome to the Game of War");
		northPanel.add(gameTitleLabel);

		playerWin = new JLabel("WIN");
		playerWin.setVisible(false);
		westPanel.add(playerWin);

		cpuWin = new JLabel("WIN");
		cpuWin.setVisible(false);
		eastPanel.add(cpuWin);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Game().setVisible(true);
			}
		});
	}

	public void shuffleDeck(ArrayList<String> deck) {
		Random rand = new Random();

		for (int i = deck.size() - 1; i > 0; --i) {
			int j = rand.nextInt(deck.size());
			String temp = deck.get(i);
			deck.set(i, deck.get(j));
			deck.set(j, temp);
		}
	}

	public void createDeck() {
		for (String suit : suit) {
			for (String rank : rank) {
				initDeck.add(rank + suit);
			}
		}

		shuffleDeck(initDeck);
		seperateDeck(initDeck);
	}

	public void seperateDeck(ArrayList<String> deck) {
		for (int i = 0; i < deck.size(); ++i) {
			if (i % 2 == 0) {
				playerCards.add(deck.get(i));
			} else {
				cpuCards.add(deck.get(i));
			}
		}
	}

	public void updateWindow() {
		totalCardsPlayer.setText("Player Total Cards: " + playerCards.size());
		totalCardsCpu.setText("CPU Total Cards: " + cpuCards.size());
	}

	public void resetGame() {
		playerCards.clear();
		cpuCards.clear();
		initDeck.clear();
		cpuHand.setText(null);
		playerHand.setText(null);
		updateWindow();
	}

	public void playHand() throws InterruptedException {
		if (playerCards.size() != 0) {
			cpuWin.setVisible(false);
			playerWin.setVisible(false);

			String playerCard = playerCards.get(0);
			int playerCardRank = getRank(playerCard.substring(0, 1));

			String cpuCard = cpuCards.get(0);
			int cpuCardRank = getRank(cpuCard.substring(0, 1));

			System.out.println("Player Rank: " + playerCardRank + " CPU Rank: " + cpuCardRank);

			playerHand.setText("Player: " + playerCards.get(0) + "  ");
			cpuHand.setText("CPU: " + cpuCards.get(0) + "  ");
			
			playerCards.remove(0);

			cpuCards.remove(0);

			if (playerCardRank > cpuCardRank) {
				playerWin.setVisible(true);
				playerCards.add(playerCard);
				playerCards.add(cpuCard);

			} else if (cpuCardRank > playerCardRank) {
				cpuWin.setVisible(true);
				cpuCards.add(playerCard);
				cpuCards.add(cpuCard);

			} else {
				String tempPlayerCard = playerCards.get(0);
				String tempCpuCard = cpuCards.get(0);
				
				while(tempPlayerCard.equals("0")) {
					tempPlayerCard = playerCards.get(0);
					int tempPlayerCardRank = getRank(playerCard.substring(0, 1));

					tempCpuCard = cpuCards.get(0);
					int tempCpuCardRank = getRank(cpuCard.substring(0, 1));
					
					cardZone.add(playerCards.get(0));
					playerCards.remove(0);
					
					cardZone.add(cpuCards.get(0));
					cpuCards.remove(0);
					
					if(tempPlayerCardRank > cpuCardRank) {
						playerWin.setVisible(true);
						playerCards.add(playerCard);
						playerCards.add(cpuCard);
						for (int i = 0; i < cardZone.size(); ++i) {
							
						}
					} else if (tempCpuCardRank > tempPlayerCardRank) {
						cpuWin.setVisible(true);
						cpuCards.add(playerCard);
						cpuCards.add(cpuCard);
					}
				}
			}

			Thread.sleep(100);
		} else {
			resetGame();
		}
	}

	public int getRank(String s) {
		if (s.equals("1")) {
			s += 0;
		}

		System.out.println(s);

		for (int i = 0; i < rank.length; ++i) {
			if (rank[i].equals(s)) {
				return i;
			}
		}

		return -1;
	}
}