package hw2;

public class BrailleUtilities {
	public static final String RAISED = "\u2022";
	public static final String UNRAISED = "\u00b7";
	public static final String LETTER_SPACER = "  ";
	public static final String WORD_SPACER = "    ";
	
	public static String translateTopLine(String text) {
		String smallBig = UNRAISED + RAISED;
		String bigSmall = RAISED + UNRAISED;
		String bigBig = RAISED + RAISED;
	
		text = text.toLowerCase().trim();
		String topLine = "";
		
		for(int i = 0; i < text.length();i++) {
			String charAtIndex = text.substring(i,i + 1);
			
			if(charAtIndex.equals(" ")) {
				topLine += WORD_SPACER; 
			} else {
				charAtIndex = charAtIndex.replaceAll("[cdfgmnpqxy]", bigBig);
				charAtIndex = charAtIndex.replaceAll("[abehkloruvz]", bigSmall);
				charAtIndex = charAtIndex.replaceAll("[ijstw]", smallBig);
				topLine += charAtIndex;
				topLine += LETTER_SPACER;
			}
		}

		return topLine.trim();
	}
	
	public static String translateMiddleLine(String text) {
		String smallBig = UNRAISED + RAISED;
		String bigSmall = RAISED + UNRAISED;
		String bigBig = RAISED + RAISED;
		String smallSmall = UNRAISED + UNRAISED;
		
		text = text.toLowerCase().trim();
		String middleLine = "";
		
		for(int i = 0; i < text.length();i++) {
			String charAtIndex = text.substring(i,i + 1);
			if(charAtIndex.equals(" ")) {
				middleLine += WORD_SPACER; 
				
			} else {
				charAtIndex = charAtIndex.replaceAll("[ghjqrtw]", bigBig);
				charAtIndex = charAtIndex.replaceAll("[bfilpsv]", bigSmall);
				charAtIndex = charAtIndex.replaceAll("[denoyz]", smallBig);
				charAtIndex = charAtIndex.replaceAll("[ackmux]", smallSmall);
				middleLine += charAtIndex;
				middleLine += LETTER_SPACER;
			}
		}
		
		return middleLine.trim();
	}
	
	public static String translateBottomLine(String text) {
		String smallSmall = UNRAISED + UNRAISED;
		String smallBig = UNRAISED + RAISED;
		String bigSmall = RAISED + UNRAISED;
		String bigBig = RAISED + RAISED;
		
		text = text.toLowerCase().trim();
		String bottomLine = "";
		
		for(int i = 0; i < text.length();i++) {
			String charAtIndex = text.substring(i,i + 1);
			if(charAtIndex.equals(" ")) {
				bottomLine += WORD_SPACER; 
			} else {
				charAtIndex = charAtIndex.replaceAll("[uvxyz]", bigBig);
				charAtIndex = charAtIndex.replaceAll("[klmnopqrst]", bigSmall);
				charAtIndex = charAtIndex.replaceAll("[w]", smallBig);
				charAtIndex = charAtIndex.replaceAll("[abcdefghij]", smallSmall);
				bottomLine += charAtIndex;
				bottomLine += LETTER_SPACER;
			}
		}

		return bottomLine.trim();
	}
	
	public static String translate(String text) {
		String translation = String.format("%s%n%s%n%s%n", translateTopLine(text), translateMiddleLine(text),translateBottomLine(text));
		
		return translation;
	}
}
