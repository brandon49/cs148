package hw2;

public class WebUtilities {
	public static String getImageLink(String path, int width, int height) {
		String htmlImageLink = "<a href=\"" + path + "\"><img src=\"" + path + "\" width=\"" + width + "\" height=\"" + height + "\"></a>";
		
		return htmlImageLink;
	}
	
	public static String getHost(String url) {
		int indexOfDoubleSlash = url.indexOf("//");
		url = url.substring(indexOfDoubleSlash + 2);
		int indexOfSingleSlash = url.indexOf("/");
		url = url.substring(0,indexOfSingleSlash);
		
		return url;
	}
	
	public static String getTitle(String htmlSource) {
		int indexOfTitle = htmlSource.indexOf("<title>") + 7;
		int indexOfSlashTitle = htmlSource.indexOf("</title>");
		String title = htmlSource.substring(indexOfTitle, indexOfSlashTitle);
		
		return title;
	}
	
	public static String invertHexColor(String htmlColor) {
		String colors = "0123456789abcdef";
		htmlColor = htmlColor.replace('#', ' ').trim();
		String invertedColor = "#";
		
		for (int i = 0; i < htmlColor.length();i++) {
			char charAtIndex = htmlColor.charAt(i);
			int indexOfCharInColors = colors.indexOf(charAtIndex);
			char charInverted = colors.charAt((colors.length() - 1) - indexOfCharInColors);
			invertedColor += "" + charInverted;
		}
		
		return invertedColor;
	}
}