package References;

public class RecursiveMethod {
	public static void main(String[] args) {
		System.out.println(abacaba("dcba"));
	}
	
	public static String abacaba(String roots) { //Recursive 
		String s = "";
		
		if (!roots.isEmpty()) {
			s += abacaba(roots.substring(1));
			s += roots.charAt(0);
			s += abacaba(roots.substring(1));
		}
		
		return s;
	}
}
