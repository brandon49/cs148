package lab8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Vocab {
	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("/Users/brandonpessman/Desktop/vocab_scores.csv");
		Scanner scan = new Scanner(file);

		double maleScoreTotal = 0;
		double femaleScoreTotal = 0;
		int nMales = 0;
		int nFemales = 0;

		while (scan.hasNextLine()) {
			String rawData = scan.nextLine();
			
			String gender = rawData.substring(0, rawData.indexOf(','));
			int score = Integer.parseInt(rawData.substring(rawData.indexOf(',') + 1));

			if (gender.equalsIgnoreCase("Female")) {
				nFemales++;
				femaleScoreTotal += score;
			} else {
				nMales++;
				maleScoreTotal += score;
			}
		}

		System.out.printf("FEMALE - Mean Score (%d Females Total): %.4f%n", nFemales, femaleScoreTotal / nFemales);
		System.out.printf("MALE - Mean Score (%d Males Total): %.4f%n", nMales, maleScoreTotal / nMales);

		scan.close();
	}
}
