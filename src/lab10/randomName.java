package lab10;

import java.util.Random;

public class randomName {
	public static void main(String[] args) {
		String[] names = { "Bob", "Sam", "Steve", "Jill", "Cam", "Jack", "Jane", "Billy", "Robert", "Carrol" };
		pick5(names);
	}

	public static void pick5(String[] names) {
		Random rand = new Random();

		for (int i = names.length - 1; i > 0; --i) {
			String temp = names[i];
			int j = rand.nextInt(i);
			names[i] = names[j];
			names[j] = temp;
		}

		for (int i = 0; i < 5; i++) {
			System.out.print(names[i] + " ");
		}
	}
}
