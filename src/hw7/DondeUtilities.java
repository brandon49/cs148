package hw7;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.regex.Pattern;

public class DondeUtilities {
	public static PlacesCache readCSV(File file) throws java.io.FileNotFoundException {
		Scanner scan = new Scanner(file);
		
		PlacesCache cache = new PlacesCache();
		
		while (scan.hasNext()) {	
			String s = scan.nextLine();

			String[] parts =  s.split(Pattern.quote("|"), -1);
		
				try {
					Place place = cache.getPlace(parts[1]);
					place.addPerson(parts[0]);
				} catch (NoSuchPlaceException e) {
					e.printStackTrace();
				}	
		}
		
		scan.close();
		return cache;
	}
	
	public static void writeKML(PlacesCache cache, File file) throws java.io.FileNotFoundException{
		PrintWriter pw = new PrintWriter(file);
		pw.print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + String.format("%n"));
		pw.print("<kml xmlns=\"http://www.opengis.net/kml/2.2\">" + String.format("%n"));
		pw.print("<Document>" + String.format("%n"));
		pw.print("<name>Donde</name>" + String.format("%n"));

		for (int i = 0; i < cache.size(); ++i) {
			pw.print(cache.get(i).toKML() + String.format("%n"));
		}
		
		pw.print("</Document>" + String.format("%n"));
		pw.print("</kml>" + String.format("%n"));
		pw.flush();
		pw.close();
	}
}
