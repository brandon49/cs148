package hw1;
import java.util.Scanner;
public class LemonAid {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		System.out.print("How many parts lemon juice? ");
		int lemonJuiceParts = in.nextInt();
		
		System.out.print("How many parts sugar? ");
		int sugarParts = in.nextInt();
		
		System.out.print("How many parts water? ");
		int waterParts = in.nextInt();
		
		System.out.print("How many cups of lemonade? ");
		int cupsOfLemonade = in.nextInt();
		
		int totalProportion = lemonJuiceParts + sugarParts + waterParts;
		
		System.out.println("Amounts (in cups):");
		double lemonJuice = cupsOfLemonade * (lemonJuiceParts / (double) totalProportion);
		System.out.println("  Lemon juice: " + lemonJuice);
		double sugar = cupsOfLemonade * (sugarParts / (double) totalProportion);
		System.out.println("  Sugar: " + sugar);
		double water = cupsOfLemonade * (waterParts / (double) totalProportion);
		System.out.println("  Water: " + water);
		
		in.close();
	}
}
