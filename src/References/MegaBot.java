package References;

import java.util.Random;

import splatbot.Action;
import splatbot.Cell;
import splatbot.SplatBotColor;

public class MegaBot {
	SplatBotColor sbc;
	splatbot.Cell[][] map;
	private int direction = 0;
	private int playerRow;
	private int playerColumn;
	private String robotName = "";
	private String enemyColor = "";
	private int nTurn = 0;
	private int[][] badSpots = new int[10][10];
	boolean switchOn = true;
	boolean takeScore = false;
	int myScore = 0;
	int enemyScore = 0;
	boolean splatAttack = false;
	
	public MegaBot(SplatBotColor sbc) {
		this.sbc = sbc;
		
		if (sbc.name().equals("RED")) {
			robotName = Cell.RED_ROBOT.name();
			enemyColor = "BLUE";
		} else {
			robotName = Cell.BLUE_ROBOT.name();
			enemyColor = "RED";
		}
	}
	
	public void getScore() {
		myScore = 0;
		enemyScore = 0;
		for (int i = 0; i < map.length; ++i) {
			for (int j = 0; j < map[i].length; ++j) {
				if (map[i][j].name().equals(sbc.name())) {
					myScore++;
				} else if (map[i][j].name().equals(enemyColor)) {
					enemyScore++;
				}
			}
		}
		
		System.out.println("My Score: " + myScore + " Enemy Score: " + enemyScore);
	}
	
	public Action getAction(splatbot.Cell leftCell, splatbot.Cell frontCell, splatbot.Cell rightCell) {
		if (takeScore) {
			takeScore = false;
			getScore();
			if (myScore < enemyScore) {
				switchOn = !switchOn;
			}
		}
		
		if (nTurn % 100 == 0) {
			nTurn++;
			takeScore = true;
			return Action.SURVEY;
		} 
	
		
		boolean isFrontSafe = isSafeMove("FRONT");
		boolean isLeftSafe = isSafeMove("LEFT");
		boolean isRightSafe = isSafeMove("RIGHT");

		Random rand = new Random();
		if (rand.nextDouble()*10 < 0) {
			if (rand.nextBoolean() == true) {
				changeDirection("LEFT");
				return Action.TURN_LEFT;
			} else {
				changeDirection("RIGHT");
				return Action.TURN_RIGHT;
			}
		}
		
		System.out.println("Front Safe: " + isFrontSafe + " Left Safe: " + isLeftSafe + " Right Safe: " + isRightSafe);
		System.out.println("Direction: " + direction + " Player Location (R/C): " + playerRow + " " + playerColumn);
		System.out.println();
		
		if (switchOn) {
			if (!frontCell.equals(Cell.ROCK) && isFrontSafe && !frontCell.equals(Cell.WALL) && frontCell.toString().equals(enemyColor)) {
				nTurn++;
				updatePlayerLocation();
				return Action.MOVE_FORWARD;
			} else if (!leftCell.equals(Cell.ROCK) && isLeftSafe && !leftCell.equals(Cell.WALL) && leftCell.toString().equals(enemyColor)) {
				nTurn++;
				changeDirection("LEFT");
				return Action.TURN_LEFT;
			} else if (!rightCell.equals(Cell.ROCK) && isRightSafe && !rightCell.equals(Cell.WALL) && rightCell.toString().equals(enemyColor)) {
				nTurn++;
				changeDirection("RIGHT");
				return Action.TURN_RIGHT;
			} else {
				 if (!frontCell.equals(Cell.ROCK) && isFrontSafe && !frontCell.equals(Cell.WALL) && frontCell.toString().equals(Cell.NEUTRAL.toString())) {
						nTurn++;
						updatePlayerLocation();
						return Action.MOVE_FORWARD;
					} else if (!leftCell.equals(Cell.ROCK) && isLeftSafe && !leftCell.equals(Cell.WALL) && leftCell.toString().equals(Cell.NEUTRAL.toString())) {
						nTurn++;
						changeDirection("LEFT");
						return Action.TURN_LEFT;
					} else if (!rightCell.equals(Cell.ROCK) && isRightSafe && !rightCell.equals(Cell.WALL) && rightCell.toString().equals(Cell.NEUTRAL.toString())) {
						nTurn++;
						changeDirection("RIGHT");
						return Action.TURN_RIGHT;
				
				} else {
					if (!frontCell.equals(Cell.ROCK) && isFrontSafe && !frontCell.equals(Cell.WALL) && frontCell.toString().equals(sbc.name())) {
						nTurn++;
						updatePlayerLocation();
						return Action.MOVE_FORWARD;
					} else if (!leftCell.equals(Cell.ROCK) && isLeftSafe && !leftCell.equals(Cell.WALL) && leftCell.toString().equals(sbc.name())) {
						nTurn++;
						changeDirection("LEFT");
						return Action.TURN_LEFT;
					} else if (!rightCell.equals(Cell.ROCK) && isRightSafe && !rightCell.equals(Cell.WALL) && rightCell.toString().equals(sbc.name())) {
						nTurn++;
						changeDirection("RIGHT");
						return Action.TURN_RIGHT;
					
						} else {
							nTurn++;
							return Action.TURN_RIGHT;
					}
				}
			}
		} else {
			if (!frontCell.equals(Cell.ROCK) && isFrontSafe && !frontCell.equals(Cell.WALL) && frontCell.toString().equals(enemyColor)) {
				nTurn++;
				updatePlayerLocation();
				return Action.MOVE_FORWARD;
			} else if (!rightCell.equals(Cell.ROCK) && isRightSafe && !rightCell.equals(Cell.WALL) && rightCell.toString().equals(enemyColor)) {
				nTurn++;
				changeDirection("RIGHT");
				return Action.TURN_RIGHT;
			} else if (!leftCell.equals(Cell.ROCK) && isLeftSafe && !leftCell.equals(Cell.WALL) && leftCell.toString().equals(enemyColor)) {
				nTurn++;
				changeDirection("LEFT");
				return Action.TURN_LEFT;

			} else {
				 if (!frontCell.equals(Cell.ROCK) && isFrontSafe && !frontCell.equals(Cell.WALL) && frontCell.toString().equals(Cell.NEUTRAL.toString())) {
						nTurn++;
						updatePlayerLocation();
						return Action.MOVE_FORWARD;
					} else if (!rightCell.equals(Cell.ROCK) && isRightSafe && !rightCell.equals(Cell.WALL) && rightCell.toString().equals(Cell.NEUTRAL.toString())) {
						nTurn++;
						changeDirection("RIGHT");
						return Action.TURN_RIGHT;
					
				 } else if (!leftCell.equals(Cell.ROCK) && isLeftSafe && !leftCell.equals(Cell.WALL) && leftCell.toString().equals(Cell.NEUTRAL.toString())) {
						nTurn++;
						changeDirection("LEFT");
						return Action.TURN_LEFT;
				} else {
					if (!frontCell.equals(Cell.ROCK) && isFrontSafe && !frontCell.equals(Cell.WALL) && frontCell.toString().equals(sbc.name())) {
						nTurn++;
						updatePlayerLocation();
						return Action.MOVE_FORWARD;
					} else if (!rightCell.equals(Cell.ROCK) && isRightSafe && !rightCell.equals(Cell.WALL) && rightCell.toString().equals(sbc.name())) {
						nTurn++;
						changeDirection("RIGHT");
						return Action.TURN_RIGHT;
					
					} else if (!leftCell.equals(Cell.ROCK) && isLeftSafe && !leftCell.equals(Cell.WALL) && leftCell.toString().equals(sbc.name())) {
						nTurn++;
						changeDirection("LEFT");
						return Action.TURN_LEFT;

						} else {
							nTurn++;
							return Action.TURN_RIGHT;
					}
				}
			}
		}
	
	}
	
	public void updatePlayerLocation() {
		if (direction == 0) {
			playerRow = clamp(playerRow - 1,0,9);
		} else if (direction == 1) {
			playerColumn = clamp(playerColumn + 1,0,9);
		} else if (direction == 2) {
			playerRow = clamp(playerRow + 1,0,9);
		} else {
			playerColumn = clamp(playerColumn - 1, 0, 9);
		}
	}
	
	public void changeDirection(String d) { //LEFT OR RIGHT
		if (d.equals("LEFT")) {
			if (direction == 0) { 
				direction = 3;
			} else {
				direction--;
			}
		} else {
			if (direction == 3) { 
				direction = 0;
			} else {
				direction++;
			}
		}
	}
	
	public boolean isSafeMove(String whichCell) {
		System.out.println(badSpots[clamp(playerRow-1,0,9)][playerColumn]);
		if (direction == 0) {
			if (whichCell.equals("FRONT") && badSpots[clamp(playerRow - 1,0,9)][playerColumn] == 1) {
				return false;
			} else if (whichCell.equals("LEFT") && badSpots[playerRow][clamp(playerColumn - 1, 0, 9)] == 1) {
				return false;
			} else if (whichCell.equals("RIGHT") && badSpots[playerRow][clamp(playerColumn + 1, 0, 9)] == 1) {
				return false;
			} else {
				return true;
			}
		} else if (direction == 1) {
			if (direction == 0) {
				if (whichCell.equals("FRONT") && badSpots[playerRow][clamp(playerColumn + 1, 0, 9)] == 1) {
					return false;
				} else if (whichCell.equals("LEFT") && badSpots[clamp(playerRow - 1, 0, 9)][playerColumn] == 1) {
					return false;
				} else if (whichCell.equals("RIGHT") && badSpots[clamp(playerRow + 1, 0, 9)][playerColumn] == 1) {
					return false;
				} else {
					return true;
				}
			}
		} else if (direction == 2) {
			if (direction == 0) {
				if (whichCell.equals("FRONT") && badSpots[clamp(playerRow + 1,0,9)][playerColumn] == 1) {
					return false;
				} else if (whichCell.equals("LEFT") && badSpots[playerRow][clamp(playerColumn + 1, 0, 9)] == 1) {
					return false;
				} else if (whichCell.equals("RIGHT") && badSpots[playerRow][clamp(playerColumn - 1, 0, 9)] == 1) {
					return false;
				} else {
					return true;
				}
			}
		} else {
			if (direction == 0) {
				if (whichCell.equals("FRONT") && badSpots[playerRow][clamp(playerColumn - 1, 0, 9)] == 1) {
					return false;
				} else if (whichCell.equals("LEFT") && badSpots[clamp(playerRow + 1, 0, 9)][playerColumn] == 1) {
					return false;
				} else if (whichCell.equals("RIGHT") && badSpots[clamp(playerRow - 1, 0, 9)][playerColumn] == 1) {
					return false;
				} else {
					return true;
				}
			}
			
			
		}
		return true;
	}
	
	public void survey(splatbot.Cell[][] map) {
		this.map = map;
		checkForBadSpots();
		findPlayer();
		findPlayerDirection();
	}
	
	public void findPlayer() {
		for (int i = 0; i < map.length; ++i) {
			for (int j = 0; j < map[i].length; ++j) {
				if (map[i][j].toString().equals(robotName)) {
					playerRow = i;
					playerColumn = j;
				}
			}
		}
	}
	
	public void findPlayerDirection() {
		if (playerRow == 9 && playerColumn == 9) {
			direction = 0;
		} else if (playerRow == 0 && playerColumn == 0) {
			direction = 2;
		}
	}
	
	public void checkForBadSpots() {
		for (int i = 0; i < map.length; ++i) {
			for (int j = 0; j < map[i].length; ++j) {
				if (map[i][j].equals(Cell.ROCK) && ((i == 9 || i == 0) || (j == 0 || j == 9))) {
					int rockRow = i;
					int rockColumn = j;
					
					for(int around = 0; around < 4; ++around) {
						if (around == 0) { /* Top of Square */
							if (map[clamp(rockRow - 2, 0, 9)][rockColumn].equals(Cell.ROCK)) {
								badSpots[clamp(rockRow - 1, 0, 9)][rockColumn] = 1;
							} 
							
							if (rockRow == 1 && rockColumn == 0 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[0][0] = 1;
							} else if (rockRow == 1 && rockColumn == 9 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[0][9] = 1;
							}
						} else if (around == 1) { /* Right of Square */
							if (map[rockRow][clamp(rockColumn + 2, 0, 9)].equals(Cell.ROCK)) {
								badSpots[rockRow][clamp(rockColumn + 1, 0, 9)] = 1;
							} 
							
							if (rockRow == 0 && rockColumn == 8 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[0][9] = 1;
							} else if (rockRow == 9 && rockColumn == 8 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[9][9] = 1;
							}
						} else if (around == 2) { /* Down of Square */
							if (map[clamp(rockRow + 2, 0, 9)][rockColumn].equals(Cell.ROCK)) {
								badSpots[clamp(rockRow + 1, 0, 9)][rockColumn] = 1;
							}
							
							if (rockRow == 8 && rockColumn == 9 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[9][9] = 1;
							} else if (rockRow == 8 && rockColumn == 0 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[9][0] = 1;
							}
						} else { /* Left of Square */
							if (map[rockRow][clamp(rockColumn - 2, 0, 9)].equals(Cell.ROCK)) {
								badSpots[rockRow][clamp(rockColumn - 1, 0, 9)] = 1;
							}
							
							if (rockRow == 0 && rockColumn == 1 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[0][0] = 1;
							} else if (rockRow == 9 && rockColumn == 1 && map[rockRow][rockColumn].equals(Cell.ROCK)) {
								badSpots[9][0] = 1;
							}
						}
					}
				
					badSpots[i][j] = 1;
				} 
			}
		}
		
		for (int i = 0; i < map.length; ++i) {
			for (int j = 0; j < map[i].length; ++j) {
				System.out.print(" [" + badSpots[i][j] + "] ");
			}
			System.out.println();
		}
		System.out.println();
		
	}
	
	public int clamp(int number, int lowerBound, int upperBound) {
		return Math.min(Math.max(number, lowerBound), upperBound);
	}
}
