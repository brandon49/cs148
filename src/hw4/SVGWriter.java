package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;


public class SVGWriter {
	public static void main(String[] args) throws IOException {
		File txtFile = new File("/Users/brandonpessman/Desktop/svgfile.txt");
		File imageFile = new File("/Users/brandonpessman/Desktop/starwars_551c43f4.jpeg");
		
		BufferedWriter w = new BufferedWriter(new FileWriter(txtFile));
		BufferedImage image = ImageIO.read(imageFile);
		
		w.write(String.valueOf(image.getWidth() / (double) image.getHeight()));
		w.newLine();
		w.write("0.0 0.0 0.0");
		w.newLine();
		
		for(int i = 0; i < image.getHeight(); ++i) {
			for(int j = 0; j < image.getWidth(); ++j) {
				Color color = new Color(image.getRGB(j, i));
				
				w.write("color " + color.getRed() / (double) 255 + " " + color.getGreen() / (double) 255 + " " + color.getBlue() / (double) 255);
				w.newLine();
				
				w.write("rectangle " + j / (double) image.getWidth() + " " + (1 - (i / (double) image.getHeight())) + " " + j / (double) image.getWidth() + " " + (1 - (i / (double) image.getHeight())));
				w.newLine();
			}
		}
		
		w.flush();
		w.close();
		

	}
}
