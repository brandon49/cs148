package JFrameMiniProjects;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class TextFieldInputChecker extends JFrame {
	private JTextField fieldName;
	private JLabel labelMessage;
	private JButton buttonSubmit;
	
	private static final long serialVersionUID = 1L;
	private final int WIDTH = 450;
	private final int HEIGHT = 100;
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TextFieldInputChecker().setVisible(true);;
			}
		});
	}
	
	public TextFieldInputChecker() {
		createView();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(WIDTH,HEIGHT));
		setLocationRelativeTo(null);
		setTitle("Text Field Demo");
		setResizable(false);
	}
	
	public void createView() {
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		
		JLabel label = new JLabel("Please Enter You're Name: ");
		panel.add(label);
		
		fieldName = new JTextField();
		fieldName.setPreferredSize(new Dimension(150,30));
		panel.add(fieldName);
		
		buttonSubmit = new JButton("Submit");
		buttonSubmit.addActionListener(e -> {
				String name = fieldName.getText();
				if(name.isEmpty()) {
					labelMessage.setText("Is your name Empty? Didn't think so!");
				} else {
					labelMessage.setText("Why, hello " + name + ". How was your day?");
				}
		});
		panel.add(buttonSubmit);
		
		labelMessage = new JLabel("Please Enter You're Name!");
		panel.add(labelMessage);
	}
}
