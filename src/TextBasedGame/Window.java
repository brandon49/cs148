package TextBasedGame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Window extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public Window() {
		createView();
	}
	
	public void createView() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBackground(Color.BLACK);
		getContentPane().add(panel);
		
		JPanel westPanel = new JPanel();
		westPanel.setLayout(new GridBagLayout());
		westPanel.setBackground(Color.BLACK);
		panel.add(westPanel, BorderLayout.WEST);
		
		GridBagConstraints gbcWest = new GridBagConstraints();
		gbcWest.gridwidth = GridBagConstraints.REMAINDER;

		GridBagConstraints gbcEast = new GridBagConstraints();
		gbcEast.gridwidth = GridBagConstraints.REMAINDER;
		gbcEast.anchor = GridBagConstraints.WEST;
		
		JPanel eastPanel = new JPanel();
		eastPanel.setLayout(new GridBagLayout());
		eastPanel.setBackground(Color.BLACK);
		panel.add(eastPanel, BorderLayout.EAST);
		
		JPanel northPanel = new JPanel();
		northPanel.setBackground(Color.BLACK);
		panel.add(northPanel, BorderLayout.NORTH);
		
		JPanel southPanel = new JPanel();
		southPanel.setBackground(Color.BLACK);
		panel.add(southPanel, BorderLayout.SOUTH);
		
		JTextArea displayTextArea = new JTextArea(18,38);
		displayTextArea.setLineWrap(true);
		displayTextArea.setWrapStyleWord(true);
		displayTextArea.setEditable(false);
		displayTextArea.append("Welcome to the game! Say 'help' for a list of commands to get started.\n");
		
		JScrollPane scrollPane = new JScrollPane(displayTextArea);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		westPanel.add(scrollPane, gbcWest);
				
		JLabel playerStatsLabel = new JLabel("--------[Player Stats]--------");
		playerStatsLabel.setForeground(Color.WHITE);
		eastPanel.add(playerStatsLabel, gbcEast);
		
		JLabel hpLabel = new JLabel("HP: ");
		hpLabel.setForeground(Color.WHITE);
		eastPanel.add(hpLabel, gbcEast);
		
		JLabel energyLabel = new JLabel("Energy: ");
		energyLabel.setForeground(Color.WHITE);
		eastPanel.add(energyLabel, gbcEast);
		
		JLabel attackLabel = new JLabel("Attack: ");
		attackLabel.setForeground(Color.WHITE);
		eastPanel.add(attackLabel, gbcEast);

		JLabel defenseLabel = new JLabel("Defense: ");
		defenseLabel.setForeground(Color.WHITE);
		eastPanel.add(defenseLabel, gbcEast);

		
		JLabel placeholder = new JLabel("                                                         ");
		eastPanel.add(placeholder, gbcEast);
		
		
		
		
		
		
		CommandControler cc = new CommandControler(displayTextArea, hpLabel, energyLabel, attackLabel, defenseLabel);	
		
		JTextField commandField = new JTextField();
		commandField.setPreferredSize(new Dimension(468,30));
		commandField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cc.getCommand(commandField.getText().split(" "));
				commandField.setText(null);
			}	
		});
		westPanel.add(commandField, gbcWest);
		
		commandField.setBackground(Color.DARK_GRAY);
		commandField.setForeground(Color.WHITE);
		
		displayTextArea.setBackground(Color.DARK_GRAY);
		displayTextArea.setForeground(Color.WHITE);
		

	}
}
