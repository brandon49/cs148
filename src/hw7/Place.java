package hw7;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import java.util.Scanner;


public class Place {
	private String placeName;
	private double latitude;
	private double longitude;
	private Set people = new Set();
	
	public Place(String placeName, double latitude, double longitude) {
		this.placeName = placeName;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Place(String placeName) throws NoSuchPlaceException {
		this.placeName = placeName;
		String data;

		try {
			data = WebUtilities.slurpURL(toGeocodeURL());
			
			Scanner s = new Scanner(data);
			latitude = s.nextDouble();
			longitude = s.nextDouble();		
			s.close();
		} catch (Exception e) {
			throw new NoSuchPlaceException(placeName);
		}
	}
	
	public String getName() {
		return placeName;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public URL toGeocodeURL() throws MalformedURLException, URISyntaxException {
		return new URI("http", "www.twodee.org", "/teaching/cs1/2017c/homework/hw7/geocode.php", "place=" + placeName, null).toURL();
	}
	
	public void addPerson(String personName) {
		people.add(personName);
	}
	
	public String toKML() {
		String s = "";
		s += "<Placemark>" + String.format("%n");
		s += "<name>" + placeName + "</name>" + String.format("%n");
		s += "<description>" + people.toString() + "</description>" + String.format("%n");
		s += "<Point><coordinates>" + String.format("%.2f,%.2f", longitude, latitude) + "</coordinates></Point>" + String.format("%n");
		s += "</Placemark>";
		return s;
	}
}
