package lab10;

import java.util.Random;
import java.util.Scanner;

public class Fill {
	private static final int WIDTH = 7;
	private static final int HEIGHT = 7;
	private static final double CHANCE_OF_OBSTACLE = .1;
	private static final int startX = (int) (WIDTH / 2);
	private static final int startY = (int) (HEIGHT / 2);

	private static int playerX = startX;
	private static int playerY = startY;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		char[][] tiles = new char[WIDTH][HEIGHT];
		newGame(tiles);

		while (!isFilled(tiles)) {
			System.out.print("> ");
			String command = in.nextLine();

			if (command.equalsIgnoreCase("n")) {
				advance(tiles, 0, -1);
			} else if (command.equalsIgnoreCase("e")) {
				advance(tiles, 1, 0);
			} else if (command.equalsIgnoreCase("s")) {
				advance(tiles, 0, 1);
			} else if (command.equalsIgnoreCase("w")) {
				advance(tiles, -1, 0);
			} else if (command.equalsIgnoreCase("reset")) {
				resetGame(tiles);
			} else if (command.equalsIgnoreCase("new")) {
				newGame(tiles);
			} else {
				System.out.println("Invalid Command!");
			}
		}

		System.out.println();
		System.out.println("You Win!");
		in.close();
	}

	public static void show(char[][] tiles) {
		System.out.println();
		for (int i = 0; i < HEIGHT; ++i) {
			for (int j = 0; j < WIDTH; ++j) {
				System.out.print(tiles[j][i] + " ");
			}
			System.out.println();
		}
	}

	public static boolean isFilled(char[][] tiles) {
		boolean isEmpty = true;
		for (int i = 0; i < WIDTH; ++i) {
			for (int j = 0; j < HEIGHT; ++j) {
				if (tiles[i][j] == '.') {
					isEmpty = false;
				}
			}
		}

		return isEmpty;
	}

	public static void advance(char[][] tiles, int deltaX, int deltaY) {
		while (tiles[playerX + deltaX][playerY + deltaY] != 'x' && tiles[playerX + deltaX][playerY + deltaY] != '*') {
			tiles[playerX][playerY] = '*';
			playerX += deltaX;
			playerY += deltaY;

			tiles[playerX][playerY] = '?';
		}

		show(tiles);
	}

	public static void drawBorder(char[][] tiles) {
		for (int i = 0; i < WIDTH; i += WIDTH - 1) {
			for (int j = 0; j < HEIGHT; ++j) {
				tiles[i][j] = 'x';
			}
		}

		for (int i = 0; i < WIDTH; ++i) {
			for (int j = 0; j < HEIGHT; j += HEIGHT - 1) {
				tiles[i][j] = 'x';
			}
		}
	}

	public static void resetGame(char[][] tiles) {
		playerX = startX;
		playerY = startY;

		for (int i = 0; i < HEIGHT; ++i) {
			for (int j = 0; j < WIDTH; ++j) {
				if (tiles[j][i] == '*') {
					tiles[j][i] = '.';
				}

				if (tiles[j][i] == '?') {
					tiles[j][i] = '.';
				}
			}
		}

		tiles[playerX][playerY] = '?';
		show(tiles);
	}

	public static char[][] newGame(char[][] tiles) {
		Random rand = new Random();
		playerX = startX;
		playerY = startY;

		for (int i = 0; i < WIDTH; ++i) {
			for (int j = 0; j < HEIGHT; ++j) {
				double random = rand.nextDouble();
				if (i == playerX && j == playerY) {
					tiles[i][j] = '?';
				} else if (random >= CHANCE_OF_OBSTACLE) {
					tiles[i][j] = '.';
				} else {
					tiles[i][j] = 'x';
				}
			}
		}

		drawBorder(tiles);
		show(tiles);

		return tiles;
	}
}
