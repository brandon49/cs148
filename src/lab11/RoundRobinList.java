package lab11;

import java.util.ArrayList;

public class RoundRobinList {
	private ArrayList<String> list;
	private int currentIndex = 0;

	public RoundRobinList() {
		list = new ArrayList<String>();
	}

	public int size() {
		return list.size();
	}

	public void add(String s) {
		list.add(0, s);
	}

	public String get() {
		String s = "";

		if (currentIndex < size()) {
			s = list.get(currentIndex);
			currentIndex++;
		} else {
			currentIndex = 0;
			s = list.get(currentIndex);
			currentIndex++;
		}

		return s;
	}
}
