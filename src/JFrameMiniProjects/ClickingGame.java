package JFrameMiniProjects;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ClickingGame extends JFrame {
	private static final long serialVersionUID = 1L;
	private final static int WIDTH = 400;
	private final static int HEIGHT = 65;

	private JButton buttonClick, buttonReset;
	private JLabel labelClicks;
	private int nClicks;

	public ClickingGame() {
		createView();

		setSize(new Dimension(WIDTH, HEIGHT));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Clicking Game Demo");
	}

	public void createView() {
		JPanel panel = new JPanel();
		getContentPane().add(panel);

		labelClicks = new JLabel();
		labelClicks.setSize(new Dimension(200, 30));
		panel.add(labelClicks);

		updateCounter();

		buttonClick = new JButton("Click");
		buttonClick.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nClicks++;
				updateCounter();
			}
		});
		panel.add(buttonClick);

		buttonReset = new JButton("Reset");
		buttonReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nClicks = 0;
				updateCounter();
			}
		});
		panel.add(buttonReset);

		isVisible();
	}

	public void updateCounter() {
		labelClicks.setText("You have " + nClicks + " clicks!");
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new ClickingGame().setVisible(true);

			}
		});
	}
}
