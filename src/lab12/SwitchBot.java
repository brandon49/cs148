package lab12;

import java.util.Random;

import splatbot.Action;
import splatbot.Cell;
import splatbot.SplatBotColor;

public class SwitchBot {
	int nTurn = 0;
	
	SplatBotColor sbc;
	
	/* Used to calculate how long I have been stuck on my own tile */
	public static int nCounter = 0; //Counts how long I have been on my own tile
	public static int activatorForTrap = 75; //When the counter hits this amount, it will trigger the trapped method
	public static int passFrequency = 500; //For when the robot gets stuck
	public static int whenToCheckForPass = 10000; //Wont check for a pass unless nTurns is greater then this
	
	public static Cell myRobot;
	public static Cell enemyRobot;
	public static Cell myTileColor;
	public static Cell enemyTileColor;
	
	boolean isSwitched; //Controls the precedence of the algorithm
	
	public SwitchBot(SplatBotColor sbc) {
		this.sbc = sbc;
		setupColors();
	}
	
	public Action getAction(splatbot.Cell leftCell, splatbot.Cell frontCell, splatbot.Cell rightCell) {
		if (nTurn > whenToCheckForPass && nTurn % passFrequency == 0) {
			nTurn++;
			return Action.PASS;
		}
		
		if (nTurn % 5 > 2) {
			isSwitched = false;
			return algorithm(leftCell, frontCell, rightCell);
		} else {
			isSwitched = true;
			return algorithm(leftCell, frontCell, rightCell);
		}	
	}
	
	public Action algorithm(splatbot.Cell leftCell, splatbot.Cell frontCell, splatbot.Cell rightCell) {
		splatbot.Cell[] precedence = new splatbot.Cell[3];
		Action[] actions = new Action[2];
		
		if (isSwitched) {
			actions[0] = Action.TURN_LEFT;
			actions[1] = Action.TURN_RIGHT;
			precedence[0] = frontCell;
			precedence[1] = leftCell;
			precedence[2] = rightCell;
		} else {
			actions[0] = Action.TURN_RIGHT;
			actions[1] = Action.TURN_LEFT;
			precedence[0] = frontCell;
			precedence[1] = rightCell;
			precedence[2] = leftCell;
		}
		
		nTurn++;
		if (!precedence[0].equals(Cell.ROCK) && !precedence[0].equals(Cell.WALL) && precedence[0].equals(enemyTileColor) && !precedence[0].equals(enemyRobot)) {
			if (precedence[0].equals(myTileColor)) {
				nCounter++;
				
				if (nCounter > activatorForTrap) {
					return trapped();
				}
			} else {
				nCounter = 0;
			}
			
			return Action.MOVE_FORWARD;
		} else if (!precedence[1].equals(Cell.ROCK) && !precedence[1].equals(Cell.WALL) && precedence[1].equals(enemyTileColor) && !precedence[1].equals(enemyRobot)){
			return actions[0];
		} else if (!precedence[2].equals(Cell.ROCK) && !precedence[2].equals(Cell.WALL) && precedence[2].equals(enemyTileColor) && !precedence[2].equals(enemyRobot)) {
			return actions[1];
		} else {
			if (!precedence[0].equals(Cell.ROCK) && !precedence[0].equals(Cell.WALL) && precedence[0].equals(Cell.NEUTRAL)) {
				if (precedence[0].equals(myTileColor)) {
					nCounter++;
					
					if (nCounter > activatorForTrap) {
						return trapped();
					}
				} else {
					nCounter = 0;
				}
				
				return Action.MOVE_FORWARD;
			} else if (!precedence[1].equals(Cell.ROCK) && !precedence[1].equals(Cell.WALL) && precedence[1].equals(Cell.NEUTRAL)){
				return actions[0];
			} else if (!precedence[2].equals(Cell.ROCK) && !precedence[2].equals(Cell.WALL) && precedence[2].equals(Cell.NEUTRAL)) {
				return actions[1];
			} else {
					if (!precedence[0].equals(Cell.ROCK) && !precedence[0].equals(Cell.WALL)) {
						if (precedence[0].equals(myTileColor)) {
							nCounter++;
							
							if (nCounter > activatorForTrap) {
								return trapped();
							}
						} else {
							nCounter = 0;
						}
			
						return Action.MOVE_FORWARD;
					} else if (!precedence[1].equals(Cell.ROCK) && !precedence[1].equals(Cell.WALL)){
						return actions[0];
					} else if (!precedence[2].equals(Cell.ROCK) && !precedence[2].equals(Cell.WALL)) {
						return actions[1];
					} else {
						return actions[0];
					}
				}	
		}
	}
	
	public void setupColors() {
		if (sbc.toString().equals("RED")) {
			myRobot = Cell.RED_ROBOT;
			enemyRobot = Cell.BLUE_ROBOT;
			
			enemyTileColor = Cell.BLUE;
			myTileColor = Cell.RED;
			
		} else {
			myRobot = Cell.BLUE_ROBOT;
			enemyRobot = Cell.RED_ROBOT;
			
			enemyTileColor = Cell.RED;
			myTileColor = Cell.BLUE;
		
		}
	}
	
	public void survey(splatbot.Cell[][] map) {
		
	}

	public static Action trapped() {
		Random rand = new Random();
		
		Action[] actions = { Action.MOVE_FORWARD, Action.MOVE_BACKWARD, Action.TURN_LEFT, Action.TURN_RIGHT };

		return actions[rand.nextInt(4)];
	}
}
