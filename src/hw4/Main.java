package hw4;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Main {
	public static void main(String[] args) throws IOException {
		File file = new File("/Users/brandonpessman/Desktop/svgfile.txt");
		BufferedImage image = Proportable.plot(file, 500);
		File outputFile = new File("/Users/brandonpessman/Desktop/starwars_thelastjedi.jpg");
		ImageIO.write(image, "jpg", outputFile);
		
	}
}
