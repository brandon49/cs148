package hw5;

import hw5.speccheck.Note;

public class MusicUtilities {
	public static Note[] getScale(Note rootNote, int[] halfstepOffsets) {
		Note[] scales = new Note[halfstepOffsets.length + 1];

		scales[0] = rootNote;

		for (int i = 1; i < scales.length; ++i) {
			scales[i] = new Note(halfstepOffsets[i - 1] + scales[i - 1].getHalfstepID(), rootNote.getDuration(),
					rootNote.getAmplitude(), rootNote.isDotted());
		}

		return scales;
	}

	public static Note[] getMajorScale(Note rootNote) {
		return getScale(rootNote, new int[] { 2, 2, 1, 2, 2, 2, 1 });
	}

	public static Note[] getMinorPentatonicBluesScale(Note rootNote) {
		return getScale(rootNote, new int[] { 3, 2, 2, 3, 2 });
	}

	public static Note[] getBluesScale(Note rootNote) {
		return getScale(rootNote, new int[] { 3, 2, 1, 1, 3, 2 });
	}

	public static Note[] getNaturalMinorScale(Note rootNote) {
		return getScale(rootNote, new int[] { 2, 1, 2, 2, 1, 2, 2 });
	}

	public static Note[] join(Note[] firstNotes, Note[] secondNotes) {
		Note[] joinedNotes = new Note[firstNotes.length + secondNotes.length];

		for (int i = 0; i < firstNotes.length; ++i) {
			joinedNotes[i] = firstNotes[i];
		}

		for (int i = 0; i < secondNotes.length; ++i) {
			joinedNotes[i + firstNotes.length] = secondNotes[i];
		}

		return joinedNotes;
	}

	public static Note[] repeat(Note[] notes, int cycles) {
		Note[] repeatedNotes = new Note[notes.length * cycles];

		for (int i = 0; i < cycles; ++i) {
			for (int j = 0; j < notes.length; ++j) {
				repeatedNotes[j + (i * notes.length)] = notes[j];
			}
		}

		return repeatedNotes;
	}

	public static Note[] ramplify(Note[] notes, double startingAmplitude, double endingAmplitude) {
		Note[] rampedNotes = new Note[notes.length];
		
		double distanceBetweenAmplitudes = endingAmplitude - startingAmplitude;
		
		for(int i = 0; i < rampedNotes.length; ++i) {
			double amplitude = startingAmplitude + (distanceBetweenAmplitudes * (i / (double) (rampedNotes.length - 1)));
			rampedNotes[i] = new Note(notes[i].getHalfstepID(), notes[i].getDuration(), amplitude, notes[i].isDotted());
		}
		
		return rampedNotes;
	}

	public static Note[] reverse(Note[] notes) {
		Note[] reversedNotes = new Note[notes.length];

		for (int i = 0; i < notes.length; ++i) {
			reversedNotes[i] = notes[(notes.length - 1) - i];
		}

		return reversedNotes;
	}

	public static Note[] transpose(Note[] notes, Note newRootNote) {
		int offset = notes[0].getHalfstepID() - newRootNote.getHalfstepID();
		Note[] transposedNotes = new Note[notes.length];

		for (int i = 0; i < transposedNotes.length; ++i) {
			transposedNotes[i] = new Note(notes[i].getHalfstepID() - offset, notes[i].getDuration(),
					notes[i].getAmplitude(), notes[i].isDotted());
		}

		return transposedNotes;
	}

	public static Note[] invert(Note[] notes, Note pivotNote) {
		Note[] invertedNotes = new Note[notes.length];
		
		for (int i = 0; i < invertedNotes.length; ++i) {
			int offset = (notes[i].getHalfstepID() - pivotNote.getHalfstepID()) * 2;
			invertedNotes[i] = new Note(notes[i].getHalfstepID() - offset, notes[i].getDuration(), notes[i].getAmplitude(), notes[i].isDotted());
		}
		
		return invertedNotes;
	}
}
