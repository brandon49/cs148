package JFrameMiniProjects;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class GameLoopTest extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GameLoopTest() {
		createView();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(600,600));
		setLocationRelativeTo(null);
		
	}
	
	public void createView() {
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		
		JLabel label = new JLabel("KJDLF");
		panel.add(label);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				GameLoopTest glt = new GameLoopTest();
				glt.setVisible(true);
				
				int i = 0;
				while(true) {
					System.out.println(i);
					i++;
					//glt.repaint();
				}
			}
			
		});
	}
}
