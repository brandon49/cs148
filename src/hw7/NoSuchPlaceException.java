package hw7;

public class NoSuchPlaceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6588500059127314364L;

	public NoSuchPlaceException(String placeName) {
		super(placeName);
	}
}
