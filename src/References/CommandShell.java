package References;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class CommandShell {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		File currentDirectory = new File(System.getProperty("user.home"));
		Desktop desktop = Desktop.getDesktop();
		
		System.out.print("> ");
		while (in.hasNext()) {
			String command = in.next();
			
			if (command.equalsIgnoreCase("where")) {
				System.out.println(currentDirectory.getAbsolutePath());
			} else if (command.equalsIgnoreCase("up")) {
				currentDirectory = currentDirectory.getParentFile();
			} else if (command.equalsIgnoreCase("ls")) {
				String[] children = currentDirectory.list();
				
				for (String child : children) {
					System.out.println(child);
				}
			} else if (command.equalsIgnoreCase("into")) {
				currentDirectory = new File(currentDirectory, in.next());
			} else if (command.equalsIgnoreCase("open")) {
				File file = new File(currentDirectory, in.next());
				desktop.open(file);
			}
			
			System.out.print("> ");
		}
	}
}
