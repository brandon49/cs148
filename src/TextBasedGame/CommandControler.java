package TextBasedGame;

import javax.swing.JLabel;
import javax.swing.JTextArea;

public class CommandControler {
	JTextArea display;
	JLabel playerHealthLabel,playerEnergyLabel,playerAttackLabel,playerDefenseLabel;
	Player player;
	
	public CommandControler(JTextArea displayTextArea, JLabel playerHealthLabel, JLabel playerEnergyLabel, JLabel playerAttackLabel, JLabel playerDefenseLabel) {
		display = displayTextArea;
		this.playerHealthLabel = playerHealthLabel;
		this.playerEnergyLabel = playerEnergyLabel;
		this.playerAttackLabel = playerAttackLabel;
		this.playerDefenseLabel = playerDefenseLabel;
	}
	
	public void getCommand(String[] s) {
		String command = s[0];
		
		if (command.equalsIgnoreCase("Help")) {
			getHelp();
		} else if (command.equalsIgnoreCase("Setup")) {
			setupCharacter(s);
		} 
		else {
			display.append("Invalid Command!\n");
		}
	}
	
	private void getHelp() {
		display.append("Here is a list of commands:\n");
		display.append("     Help - Allows you to see this list.\n");
		display.append("     Setup [name] - Sets up your character.\n");
		display.append("     Explore [Location] - Allows you to find things.\n");
	}
	
	private void setupCharacter(String[] s) {
		if(player.equals(null)) {
			player = new Player(s[1]);
		}
	}
}
