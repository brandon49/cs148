package lab7;

import java.util.Scanner;

public class Ordinal {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean isRunning = true;
		while(isRunning) {
			
			System.out.print("> ");
			String s = in.nextLine();
			
			if (s.equalsIgnoreCase("end")) {
				isRunning = false;
			} else {
				int number = Integer.parseInt(s);
				String ordinalNumber = getOrdinal(number);
				System.out.println(ordinalNumber);
			}
		}
		
		in.close();
	}
	
	public static String getOrdinal(int number) {
		int tempNumber = number;
		
		number %= 100;
		
		System.out.println(number);
		String newNumber;
		
		if (number >= 11 && number <= 13) {
			newNumber = Integer.toString(tempNumber) + "th";
		} else {
			number = number % 10;
			
			if (number == 2){
				newNumber = Integer.toString(tempNumber) + "nd";
			} else if (number == 3) {
				newNumber = Integer.toString(tempNumber) + "rd";
			} else if (number == 1) {
				newNumber = Integer.toString(tempNumber) + "st";
			} else {
				newNumber = Integer.toString(tempNumber) + "th"; 
			}
		}
		
		return newNumber;
	}
}
