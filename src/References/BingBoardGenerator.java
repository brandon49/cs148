package References;

import java.util.Random;

public class BingBoardGenerator {
	public static void main(String[] args) {
		Random rand = new Random();
		System.out.println("|  B  |  I  |  N  |  G  |  O  |");
		for(int i = 0; i < 5; ++i) {
			for(int j = 0; j < 5; ++j) {
				int min = 15*j + 1;
				int max = 15*(j + 1);
				int number = rand.nextInt((max - min) + 1) + min;
				System.out.print("|  " + number + " ");
			}
			
			System.out.print(" |");
			System.out.println();
		}
	}
}
