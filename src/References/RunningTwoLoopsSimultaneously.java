package References;


public class RunningTwoLoopsSimultaneously {
	public static void main(String[] args) {
		Countdown a = new Countdown("Thread A");
		Countdown b = new Countdown("Thread B");
	
		a.start();
		b.start();
		System.out.println("END");
	}
}

class Countdown extends Thread {
	private String name;
	
	public Countdown(String givenName) { //Constructor
		name = givenName;
	}
	
	public void run() {
		//Simultaneous job!
		for(int i = 10; i >= 0; --i) {
			System.out.println(name + i);
		}
	}
}
