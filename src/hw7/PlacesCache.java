package hw7;

import java.util.ArrayList;

public class PlacesCache {
	private ArrayList<Place> places = new ArrayList<>();
	
	public PlacesCache() {

	}
	
	public boolean isCached(String placeName) {
		for (int i = 0; i < places.size(); ++i) {
			if (places.get(i).getName().equals(placeName)) {
				return true;
			}
		}
		
		return false;
	}
	
	public Place getPlace(String placeName) throws NoSuchPlaceException {
		if (isCached(placeName)) {
			for (int i = 0; i < places.size(); ++i) {
				if (places.get(i).getName().equals(placeName)) {
					return places.get(i);
				}
			}
		}
		
		Place place = new Place(placeName);
		places.add(place);
		return place;
	}
	
	public int size() {
		return places.size();
	}
	
	public Place get(int i) {
		return places.get(i);
	}
}
