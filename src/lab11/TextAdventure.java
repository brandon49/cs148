package lab11;

import java.util.Scanner;

public class TextAdventure {
	private static boolean isRunning = true;
	private static boolean hasSword = false;

	public static void main(String[] args) {
		Room center = new Room("You look around, curious as to where you are. From the looks of it, you are "
				+ "stuck in a store basement, again. There are four tunnels (n,w,s,e). \nWhere do you go? "
				+ "(Say 'pickup' to pickup the mighty sword on the floor!)");

		Room north = new Room("You travel north and as you do, you notice a lever. You pull it. A Door "
				+ "from behind you comes sliding up! \nThere is no going back. Travel 'n' to continue your journey!");

		Room south = new Room("You travel south. A mysterious man is sitting in the corner. He looks at "
				+ "you in a creepy looking way. You get the idea that you shouldnt be here... ");

		Room east = new Room("You travel east. You find lots of gold and jewels! You are the richest of them all! \n"
				+ "Then you realize you have nothing to carry all this gold with.... You can only carry "
				+ "about 5 coins... Better luck next time!");

		Room west = new Room("You travel west. You find water and a fishing pole. You decide to fish in the hopes "
				+ "of catching some food. Instead, you only manage to catch a boot.");

		Room bossRoom = new Room("You continued north. You find a big dragon waiting at the end! Theres "
				+ "only one thing to do! (Fight/Run)");

		center.setNorth(north);
		center.setSouth(south);
		center.setEast(east);
		center.setWest(west);
		north.setNorth(bossRoom);
		east.setWest(center);
		south.setNorth(center);
		west.setEast(center);

		Room currentRoom = center;
		Scanner in = new Scanner(System.in);

		System.out.println(currentRoom.getRoomDescription());
		while (isRunning) {
			System.out.print("> ");
			String command = in.nextLine().trim();

			if (command.equalsIgnoreCase("n") && currentRoom.getNorth() != null) { /*--- North ---*/
				currentRoom = currentRoom.getNorth();

				System.out.println("");
				System.out.println(currentRoom.getRoomDescription());
			} else if (command.equalsIgnoreCase("e") && currentRoom.getEast() != null) { /*--- East ---*/
				currentRoom = currentRoom.getEast();

				System.out.println("");
				System.out.println(currentRoom.getRoomDescription());
			} else if (command.equalsIgnoreCase("s") && currentRoom.getSouth() != null) { /*--- South ---*/
				currentRoom = currentRoom.getSouth();

				System.out.println("");
				System.out.println(currentRoom.getRoomDescription());
			} else if (command.equalsIgnoreCase("w") && currentRoom.getWest() != null) { /*--- West ---*/
				currentRoom = currentRoom.getWest();

				System.out.println("");
				System.out.println(currentRoom.getRoomDescription());
			} else if (currentRoom == bossRoom && command.equalsIgnoreCase("fight")) { /*--- Fight Command ---*/
				System.out.println("");

				if (hasSword) {
					System.out.println("You fight the mighty dragon by jumping on his back and "
							+ "slaying him with your MIGHTY SWORD, right in the FACE! You win.");
				} else {
					System.out.println("You fight the mighty dragon with your BARE HANDS! He doesnt "
							+ "feel a thing. \nIt doesnt work out too well, but the dragon feels bad"
							+ " and lets you pass anyways. You win, but without the achievement of slaying a dragon!");
				}
				isRunning = false;
			} else if (currentRoom == bossRoom && command.equalsIgnoreCase("run")) { /*--- Run Away Command ---*/
				System.out.println("");
				System.out.println("You run away, but there is no where to run. The mighty dragon hits you with his"
						+ " tiny fireballs and you die. You Lose.");
				
				isRunning = false;
			} else if (currentRoom == center && command.equalsIgnoreCase("pickup") && !hasSword) { /*--- Pickup Command ---*/
				System.out.println();
				hasSword = true;
				System.out.println(
						"You pickup the MIGHTY SWORD. This may be helpful. " + "Which tunnel will take? (n,w,e,s)");
			
			} else {
				System.out.println("");
				System.out.println("That command was either not found or the room isnt there! Try again...");
			}
		}

		in.close();
	}
}
