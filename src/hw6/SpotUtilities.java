package hw6;

import java.io.File;

import hw6.speccheck.GifSequenceWriter;

public class SpotUtilities {
	public static int[] countNeighbors(boolean[][] bitmap, int outerHorizontalRadius, int outerVerticalRadius,
			int innerHorizontalRadius, int innerVerticalRadius, int columnIndex, int rowIndex) {

		int activators = 0;
		int inhibitors = 0;

		for (int i = rowIndex - outerVerticalRadius; i <= rowIndex + outerVerticalRadius; i++) {
			for (int j = columnIndex - outerHorizontalRadius; j <= columnIndex + outerHorizontalRadius; j++) {
				int deltaRow = Math.abs(rowIndex - i);
				int deltaColumn = Math.abs(columnIndex - j);

				double outerDistance = ((Math.pow(deltaColumn, 2)) / (double) (Math.pow(outerHorizontalRadius, 2)))
						+ ((Math.pow(deltaRow, 2)) / (double) (Math.pow(outerVerticalRadius, 2)));
				double innerDistance = ((Math.pow(deltaColumn, 2)) / (double) (Math.pow(innerHorizontalRadius, 2))
						+ ((Math.pow(deltaRow, 2)) / (double) (Math.pow(innerVerticalRadius, 2))));

				if (outerDistance <= 1 && innerDistance <= 1 && BitmapUtilities.isOn(bitmap, j, i)) {
					activators++;
				} else if (outerDistance <= 1 && innerDistance > 1 && BitmapUtilities.isOn(bitmap, j, i)) {
					inhibitors++;
				}
			}
		}

		return new int[] { activators, inhibitors };
	}

	public static boolean[][] step(boolean[][] bitmap, int outerHorizontalRadius, int outerVerticalRadius,
			int innerHorizontalRadius, int innerVerticalRadius, double proportion) {
		
		boolean[][] newBitmap = new boolean[bitmap.length][bitmap[0].length];

		for (int i = 0; i < newBitmap.length; ++i) {
			for (int j = 0; j < newBitmap[0].length; ++j) {
				int[] count = countNeighbors(bitmap, outerHorizontalRadius, outerVerticalRadius, innerHorizontalRadius,
						innerVerticalRadius, j, i);
				double difference = count[0] - (proportion * count[1]);

				if (Math.abs(difference) < .001) {
					newBitmap[i][j] = bitmap[i][j];
				} else if (difference > 0) {
					newBitmap[i][j] = true;
				} else {
					newBitmap[i][j] = false;
				}
			}
		}

		return newBitmap;
	}

	public static int converge(boolean[][] bitmap, int outerHorizontalRadius, int outerVerticalRadius,
			int innerHorizontalRadius, int innerVerticalRadius, double proportion, File file, int maxSteps) {
		
		GifSequenceWriter out = new GifSequenceWriter(file, 200, true);

		boolean[][] preBitmap = BitmapUtilities.clone(bitmap);
		boolean[][] postBitmap = BitmapUtilities.clone(bitmap);

		out.appendFrame(BitmapUtilities.toBufferedImage(preBitmap));

		int nSteps = 1;
		boolean isNotSame = true;

		while (nSteps <= maxSteps && isNotSame) {
			postBitmap = step(preBitmap, outerHorizontalRadius, outerVerticalRadius, innerHorizontalRadius,
					innerVerticalRadius, proportion);

			out.appendFrame(BitmapUtilities.toBufferedImage(postBitmap));

			if (BitmapUtilities.equals(preBitmap, postBitmap)) {
				isNotSame = false;
			}

			preBitmap = BitmapUtilities.clone(postBitmap);

			++nSteps;
		}

		out.close();

		return nSteps;
	}
}
