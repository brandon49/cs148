package hw7;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class WebUtilities {
	public static String slurpURL(URL url) throws IOException {
		String s = "";
		URLConnection conn = url.openConnection();
		InputStream is = conn.getInputStream();
		Scanner in = new Scanner(is);
		
		while(in.hasNextLine()) {
			s += in.nextLine() + String.format("%n");
		}
		
		in.close();
		return s;
	}
}
