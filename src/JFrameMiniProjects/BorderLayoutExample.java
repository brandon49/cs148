package JFrameMiniProjects;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class BorderLayoutExample extends JFrame {
	private static final long serialVersionUID = 1L;
	private final int WIDTH = 500;
	private final int HEIGHT = 500;
	
	public BorderLayoutExample() {
		createView();
		
		setSize(new Dimension(WIDTH,HEIGHT));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("Warmpup");
	}
	
	public void createView() {
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout());
		
		
		JPanel northPanel = new JPanel();
		panel.add(northPanel, BorderLayout.NORTH);
		
		JPanel southPanel = new JPanel();
		panel.add(southPanel, BorderLayout.SOUTH);
		
		JPanel westPanel = new JPanel();
		panel.add(westPanel, BorderLayout.WEST);
		
		JPanel eastPanel = new JPanel();
		panel.add(eastPanel, BorderLayout.EAST);
		
		JPanel centerPanel = new JPanel();
		panel.add(centerPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("Hello World");
		westPanel.add(label);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new BorderLayoutExample().setVisible(true);
			}
			
		});
	}
}
