package hw3;

import java.io.File;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Trutilities {
	public static boolean isOrdered(int a, int b, int c, int d, int e) {
		return (a <= b && b <= c && c <= d && d <= e) || (a >= b && b >= c && c >= d && d >= e);
	}

	public static boolean isGreenish(String s) {
		String hexAlphabet = "0123456789abcdef";
		String hex = s.substring(1).trim();

		String red = hex.substring(0, 2);
		String green = hex.substring(2, 4);
		String blue = hex.substring(4, 6);

		return (hexAlphabet.indexOf(red.charAt(0)) * 16
				+ hexAlphabet.indexOf(red.charAt(1))) < (hexAlphabet.indexOf(green.charAt(0)) * 16
						+ hexAlphabet.indexOf(green.charAt(1)))
				&& (hexAlphabet.indexOf(blue.charAt(0)) * 16
						+ hexAlphabet.indexOf(blue.charAt(1))) < (hexAlphabet.indexOf(green.charAt(0)) * 16
								+ hexAlphabet.indexOf(green.charAt(1)));
	}

	public static boolean isMilitary(String s) {
		return Integer.parseInt(s.substring(0, 2)) <= 23 && Integer.parseInt(s.substring(0, 2)) >= 0
				&& Integer.parseInt(s.substring(2, 4)) <= 59 && Integer.parseInt(s.substring(2, 4)) >= 0;
	}

	public static boolean isImage(File file) {
		String fileExtension = file.getName().toLowerCase();

		return !(fileExtension.indexOf('.') == -1)
				&& (fileExtension.substring(fileExtension.lastIndexOf('.')).equals(".png")
						|| fileExtension.substring(fileExtension.lastIndexOf('.')).equals(".gif")
						|| fileExtension.substring(fileExtension.lastIndexOf('.')).equals(".jpg")
						|| fileExtension.substring(fileExtension.lastIndexOf('.')).equals(".jpeg"));
	}

	public static boolean hasMultipleDots(String s) {
		return s.indexOf('.') != -1 && (s.indexOf('.') != s.lastIndexOf('.'));
	}

	public static boolean fitsAspect(int width, int height, double ratio) {
		return Math.abs((width / (double) height) - ratio) >= 0 && Math.abs((width / (double) height) - ratio) <= 0.001;
	}

	public static boolean fitsWithin(int widthA, int heightA, int widthB, int heightB) {
		return widthA * heightA <= widthB * heightB;
	}

	public static boolean isFaster(String timeA, String timeB) {
		Scanner aTime = new Scanner(timeA);
		aTime.useDelimiter(":");
		double aHr = aTime.nextDouble();
		double aMin = aTime.nextDouble();
		aTime.close();

		Scanner bTime = new Scanner(timeB);
		bTime.useDelimiter(":");
		double bHr = bTime.nextDouble();
		double bMin = bTime.nextDouble();
		bTime.close();

		return aHr * 60 + aMin < bHr * 60 + bMin;
	}

	public static boolean isIllegal(String s) {
		String direction = s.toLowerCase();
		return !(direction.equals("north") || direction.equals("east") || direction.equals("south")
				|| direction.equals("west"));
	}

	public static boolean isOld(File file, int year, int month, int day) {
		return file.lastModified() <= new GregorianCalendar(year, month - 1, day).getTimeInMillis();
	}
}
