package hw5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import hw5.speccheck.Duration;
import hw5.speccheck.Note;
import hw5.speccheck.WavIO;

public class Main {
	public static void main(String[] args) throws FileNotFoundException {
		WavIO wavOutput = new WavIO();
		File file = new File("/Users/brandonpessman/Desktop/jamesbondtheme.txt");
		
		Scanner noteCounter = new Scanner(file);
		noteCounter.useDelimiter(",");
		
		Scanner in = new Scanner(file);
		in.useDelimiter(",");
		
		int nNotes = 0;

		while(noteCounter.hasNext()) {
			String s = noteCounter.next().trim();
			if(s.charAt(0) != '[') {
				++nNotes;
			}
		}

		Note[] track = new Note[nNotes];

		int n = 0;
				
		while(in.hasNext()) {
			String s = in.next().trim();
			if(s.charAt(0) != '[') {
				if(s.equals("QR")) {
					track[n] = new Note(0, Duration.QUARTER, 0, false);
					++n;
				} else {
					track[n] = new Note(s);
					++n;
				}
			}
		}
		
		noteCounter.close();
		in.close();
		
		for(int i = 0; i < track.length; ++i) {
			System.out.println(track[i]);
		}
		
		wavOutput.write(track, 120, "/Users/brandonpessman/Desktop/programmed_jamesbond.wav");
	}
}
